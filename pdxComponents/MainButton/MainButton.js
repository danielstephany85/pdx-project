import React from 'react';
import './mainButton.scss';

const MainButton = function(props) {
    const {className, children} = props,
    baseClass = 'main-button',
    classes = className ? baseClass + ' ' + className : baseClass; 

    return <button className={classes} >{children}</button>
}

export default MainButton;