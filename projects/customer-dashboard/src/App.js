import React from 'react';
import MainButton from '@pdxComponents/MainButton';

const App = (props) => {

    return (
        <div>
            <h1>Welcome to the Customer Dashboard</h1>
            <MainButton>hola shared button!!!</MainButton>
        </div>
    );
}

export default App;