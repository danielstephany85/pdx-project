const path = require('path');
const getStyleLoaders = require('./utils/getStyleLoaders.js');

module.exports = env => {
    const isDevMode = env.development ? true : false;
    console.log(isDevMode);
    return {
        mode: isDevMode ? 'development' : 'production',
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.m?js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                            plugins: ['@babel/plugin-transform-runtime', '@babel/plugin-proposal-class-properties', '@babel/plugin-transform-react-jsx']
                        }
                    }
                },
                {
                    test: /\.css$/,
                    use: getStyleLoaders({
                        importLoaders: 1
                    }, isDevMode)
                },
                {
                    test: /\.scss$/,
                    use: getStyleLoaders({
                        importLoaders: 2,
                        sourceMap: true
                    }, isDevMode, 'sass-loader')
                },
                {
                    test: /\.(png|svg|jpg|gif)$/,
                    use: {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'assets/images',
                        }
                    }
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'assets/fonts',
                        }
                    }
                }
            ]
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    vendors: {
                        test: /[\\/]node_modules[\\/]/,
                        name: 'vendors',
                        chunks: "all"
                    }
                }
            }
        },
        resolve: {
            alias: {
                "@pdxComponents": path.resolve(__dirname, '../../pdxComponents')
            }
        },
        watchOptions: {
            ignored: /node_modules/
        }
    }
};