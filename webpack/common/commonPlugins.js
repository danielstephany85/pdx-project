
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

module.exports = [
    new CaseSensitivePathsPlugin(),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
        filename: "css/[name].min.css"
    }),
    new OptimizeCssAssetsPlugin(),
];