const path = require('path');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const commonPlugins = require('./common/commonPlugins');
const config = require('./common/webpack.config');

module.exports = env => merge(config(env), {
    entry: {
        app: './projects/customer-dashboard/src/index.js'
    },
    output: {
        filename: "js/[name].js",
        path: path.resolve(__dirname, '../projects/customer-dashboard/dist')
    },
    plugins: [
        ...commonPlugins,
        new HtmlWebpackPlugin({
            title: 'customer-dashboard',
            filename: 'index.html',
            template: path.resolve(__dirname, '../projects/customer-dashboard/public/index.html'),
            chunks: ['app']
        }),
    ],
    devServer: {
        contentBase: path.join(__dirname, 'projects/customer-dashboard/dist'),
        port: 3000,
        hot: true
    },
});